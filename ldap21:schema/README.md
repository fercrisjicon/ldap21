# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **edtasixm06/ldap21:schema** Servidor LDAP amb la base de dades edt.org.
   S'ha fet el següent:
	* futbolistaA.schema Derivat de instOrgPerson, structural, injectat dades
	  de dades-futbolA.ldif
	* futbolistaB.schema Structural
	* futbolistaC.schema Auxiliary
			
```
docker network create hisix2

- Muntar l'imatge:
docker build -t cristiancondolo21/ldap21:schema .
- Arrencar-la iteractivament:
docker run --rm --name ldap.edt.org -h ldap.edt.org --net hisx2 -p 389:389 -it edtasixm06/ldap21:schema /bin/bash

- Montar la nova schema futbolistaC.schema:
bash startup.sh
- Afegir els jugadors data-futbolC.sh:
ldapadd -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f data-futbolC.ldif

- Arrencar el PHP de ldap:
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisix -p 80:80 -d edtasixm06/phpldapadmin:20

- Anar a un navegador i entrar dins del PHP:
localhost/phpldapadmin
```
**EN CAS DE QUE: et doni un error dient que el port 80 esta ocupat(already), utilitza un altre port com el 5000.**
**En el navegador, obre el phpldapadmin desde aquell port.** 
```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisix -p 5000:80 -d edtasixm06/phpldapadmin:20
localhost:5000/phpldapadmin
```
