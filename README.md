# LDAP @edt ASIX M06-ASO
## Curs 2021-2022

* **isx41016667/ldap21:base** Maquina LDAP de treball, base de ldap.
	Organitzacio: edt.org
* **isx41016667/ldap21:version1** Maquina LDAP base de dades amb tots els usuaris
	ldal amb base de dades.
	Organitzacio: edt.org
* **isx41016667/ldap21:editat** Maquina LDAP amb modificacions a la base de dades
	Organitzacio: edt.org
* **isx41016667/ldap21:schema** Maquina LDAP amb una nova schema de futbolistes a la base de dades
	Organitzacio: edt.org
* **isx41016667/ldap21:practica** Maquina LDAP de practica amb nou schema,entitats i atribut.
	Organitzacio: edt.org
* **isx41016667/ldap21:acl** Maquina LDAP de practica de permissos de modificacion de dades de la base de dades
	Organitzacio: edt.org
* **isx41016667/ldap21:grups** Maquina LDAP de treball. Amb nou ou=grups i nous entitats.
	Organitzacio: edt.org
